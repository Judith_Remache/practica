package practicasapp.judithremaxhe.facci.practicas1234;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,FrgUno.OnFragmentInteractionListener,FrDos.OnFragmentInteractionListener {

    Button buttonLogin, buttonRegistrar, buttonBuscar,botonParametro,botonFragUno,botonFragDos;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent=new Intent(MainActivity.this,ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent=new Intent(MainActivity.this,ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionLogi:
                Dialog dialogoLogi = new Dialog(MainActivity.this);
                dialogoLogi.setContentView(R.layout.dig_log);

                Button botonAuten=(Button)dialogoLogi.findViewById(R.id.btnAuten);
                final EditText cajaUser = (EditText)dialogoLogi.findViewById(R.id.txtUser);
                final EditText cajaPassword = (EditText)dialogoLogi.findViewById(R.id.txtPassword);

                botonAuten.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,cajaUser.getText().toString()+
                        ""+cajaPassword.getText().toString(),Toast.LENGTH_LONG);
                    }
                });

                dialogoLogi.show();
                break;
            case R.id.opcionRegistra:
                intent=new Intent(MainActivity.this,ActividadRegistrar.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLogin = (Button) findViewById(R.id.btnLogin);
        buttonRegistrar = (Button) findViewById(R.id.btnGuardar);
        buttonBuscar = (Button) findViewById(R.id.btnBuscar);
        botonParametro= (Button) findViewById(R.id.btnPsarParametro);
        botonFragUno=(Button)findViewById(R.id.btnFrUno);
        botonFragDos=(Button)findViewById(R.id.btnFrDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);

        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadPasarParametro.class);
                startActivity(intent);
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadLogin.class);
                startActivity(intent);
            }
        });
        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadRegistrar.class);
                startActivity(intent);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ActividadBuscar.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrUno:
                FrgUno fragmentoUno=new FrgUno();
                FragmentTransaction transactionUno =getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrDos:
                FrDos fragmentoDos=new FrDos();
                FragmentTransaction transactionDos =getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;

        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
